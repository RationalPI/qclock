#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QEvent>
#include <QApplication>
#include <QStyle>
#include <QMenu>
#include <QTime>
#include <QGraphicsDropShadowEffect>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	auto menu=[this](const QPoint &pos={}){
		QMenu m;
		connect(m.addAction("Exit"),&QAction::triggered,[]{exit(0);});
		connect(m.addAction("UpRight"),&QAction::triggered,	[this]{ui->label->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);});
		connect(m.addAction("DownRight"),&QAction::triggered,	[this]{ui->label->setAlignment(Qt::AlignRight|Qt::AlignBottom|Qt::AlignTrailing);});
		connect(m.addAction("DownLeft"),&QAction::triggered,	[this]{ui->label->setAlignment(Qt::AlignLeft|Qt::AlignBottom);});
		connect(m.addAction("UpLeft"),&QAction::triggered,		[this]{ui->label->setAlignment(Qt::AlignLeft|Qt::AlignTop);});
		m.exec(QCursor::pos());
	};

	/*Overlay with tray acone exit*/{
		setStyleSheet("background:transparent;");
		setAttribute(Qt::WA_TranslucentBackground);
		setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint|Qt::ToolTip);
		trayIcon=new QSystemTrayIcon(this);
		trayIcon->setIcon(QApplication::style()->standardIcon(QStyle::SP_MessageBoxCritical));
		trayIcon->setToolTip("Clock");
		trayIcon->setVisible(true);
		connect(trayIcon,&QSystemTrayIcon::activated,menu);
	}

	QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect(this);
	effect->setOffset(0,0);
	effect->setBlurRadius(10);
	effect->setColor(Qt::black);
	ui->label->setGraphicsEffect(effect);


	ui->label->setContextMenuPolicy(Qt::ContextMenuPolicy::CustomContextMenu);
	connect(ui->label,&QWidget::customContextMenuRequested,menu);

	/*timer updating and drawing*/{
		auto aze=[this]{
			ui->label->setText(/*must use html or reimplement paint event ....*/"<font color=\"yellow\">"+QTime::currentTime().toString("hh:mm")+"</font>");
		};
		aze();
		t.setSingleShot(false);
		t.setInterval(1000*60);
		connect(&t,&QTimer::timeout,aze);
		t.start();
	}
}

MainWindow::~MainWindow()
{
	delete ui;
}

